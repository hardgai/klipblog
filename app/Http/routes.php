<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::auth();

Route::get('/', 'CurrencyController@home')->name('home');

/**--------------------------------------
|  USER ROUTES
----------------------------------------*/
Route::group(['prefix' => 'user', 'middleware' => 'auth'], function () {

    //Route::get('/posts', 'PostController@userUploadedPosts');

    Route::get('/edit', 'UserController@edit');
    Route::post('/{user_id}/edit', 'UserController@update');
});

Route::group(['prefix' => 'currency', 'middleware' => 'auth'], function () {
    Route::get('/convert', 'CurrencyController@home');
    Route::post('/convert', 'CurrencyController@convert');
});

/*Route::group(['prefix' => 'posts', 'middleware' => 'auth'], function () {

    Route::get('/create', 'PostController@create');

    Route::post('/create', 'PostController@store');
});*/

