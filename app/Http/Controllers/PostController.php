<?php

namespace WildFusion\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use WildFusion\post;
use WildFusion\User;
use WildFusion\Http\Requests\StorePostRequest;
use WildFusion\Http\Requests;
use Illuminate\Contracts\Filesystem\Filesystem;

use WildFusion\Repositories\Posts\RepositoryInterface as PostRepInt;
use WildFusion\Repositories\Users\RepositoryInterface as UserRepInt;

class PostController extends Controller
{

    protected
        $post,
        $user;

    public function __construct(PostRepInt $post, UserRepInt $user)
    {
        $this->user = $user;
        $this->post = $post;
    }

    public function index()
    {
        $posts = $this->post->simplePaginate(16);
        return view('post.home', [
            'posts' => $posts
        ]);
    }

    public function view($postId)
    {
        $post = $this->post->find($postId);
        return view('post.single', [
            'post' => $post
        ]);
    }

    public function create()
    {
        return view('post.create');
    }

    public function store(StorePostRequest $request)
    {
        $image = $request->file('image');
        $imageName = time().'.'.$image->getClientOriginalExtension();

        $s3 = \Storage::disk('s3');
        $filePath = '/test-images/' . $imageName;
        $s3->put($filePath, file_get_contents($image), 'public');

        $imageUrl = \Storage::disk('s3')->url('test-images/' . $imageName);

        $this->post->create([
            'user_id' => Auth::user()->id,
            'description' => $request->input('description'),
            'content' => $request->input('content'),
            'image_url' => $imageUrl
        ]);

        $request->session()->flash('success', 'Post Added Successfully');
        return redirect('/posts/create');
    }

    public function userUploadedPosts()
    {
        $user = $this->user->find(Auth::user()->id);
        return view('post.home', [
                'posts' => $user->posts
            ]);
    }
}
