<?php

namespace WildFusion\Http\Controllers;

use Illuminate\Http\Request;
use WildFusion\Http\Requests\UpdateUserRequest;
use Illuminate\Support\Facades\Auth;
use WildFusion\User;
use WildFusion\Http\Requests;

use WildFusion\Repositories\Posts\RepositoryInterface as PostRepInt;
use WildFusion\Repositories\Users\RepositoryInterface as UserRepInt;

class UserController extends Controller
{

    protected
        $post,
        $user;

    public function __construct(PostRepInt $post, UserRepInt $user)
    {
        $this->user = $user;
        $this->post = $post;
    }

    public function edit()
    {
        $user = $this->user->find(Auth::user()->id);
        return view('user.edit_profile', compact('user', $user));
    }

    public function update(UpdateUserRequest $request, $id)
    {
        $user = $this->user->find($id);
        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->email = $request->input('email');

        if ($user->save())
            $request->session()->flash('success', 'Profile was successful updated!');

        return redirect('/user/edit');
    }

}
