<?php

namespace WildFusion\Http\Controllers;

use Illuminate\Http\Request;

use WildFusion\Http\Requests;
use WildFusion\Http\Requests\CurrencyRequest;

use GuzzleHttp\Client as Client;

class CurrencyController extends Controller
{

    public function index()
    {
        return view('currency.index');
    }

    public function home()
    {
        $client = new Client();
        $val = $client->request('GET', 'http://free.currencyconverterapi.com/api/v3/currencies');
        $currencies = json_decode($val->getBody(), true);
        return view('currency.home', [
            'currency' => $currencies['results']
        ]);
    }

    public function convert(CurrencyRequest $request)
    {
        $amount_1 = $request->input('amount');
        $cur_1 = $request->input('cur_1');
        $cur_2 = $request->input('cur_2');

        $param = $cur_1.'_'.$cur_2;
        $client = new Client();
        $url = "http://free.currencyconverterapi.com/api/v3/convert?q=".$param;
        $val = $client->request('GET', $url);
        $response = json_decode($val->getBody(), true);
        $result = $response['results'];
        $amount = $result[$param]['val'] * $amount_1;
        $res = [
            'from' => $cur_1,
            'to' => $cur_2,
            'amount' => $amount
        ];

        return view('currency.result', [
            'result' => $res
        ]);
    }
}
