<?php

namespace WildFusion\Http\Requests;

use WildFusion\Http\Requests\Request;

class StorePostRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                 'description' => 'required|string|max:255',
                 'content' => 'required|string:500',
                 'image' => 'required|image',
        ];
    }
}
