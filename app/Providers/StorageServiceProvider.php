<?php

namespace WildFusion\Providers;

use Illuminate\Support\ServiceProvider;

class StorageServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'WildFusion\Repositories\Posts\RepositoryInterface',
            'WildFusion\Repositories\Posts\EloquentRepository'
        );

        $this->app->bind(
            'WildFusion\Repositories\Users\RepositoryInterface',
            'WildFusion\Repositories\Users\EloquentRepository'
        );
    }
}
