<?php

namespace WildFusion;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description', 'content', 'image_url', 'user_id'
    ];

    /**
     * Inverse User-Video Relationship.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('WildFusion\User');
    }
}
