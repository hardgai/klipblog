<?php

namespace WildFusion\Repositories\Posts;

interface RepositoryInterface {
    public function all();
    public function find($id);
    public function create($input);
    public function where($search, $value);
    public function simplePaginate($count);
}
