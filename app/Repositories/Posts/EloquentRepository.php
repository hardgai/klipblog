<?php

namespace WildFusion\Repositories\Posts;

use WildFusion\Repositories\Posts\RepositoryInterface;

use WildFusion\Post;

class EloquentRepository implements RepositoryInterface {

    public function all()
    {
        return Post::all();
    }
    public function find($id)
    {
        return Post::find($id);
    }
    public function create($input)
    {
        return POst::create($input);
    }

    public function where($search, $value, $paginate = null)
    {
        if ($paginate)
            return Post::where($search, $value)->simplePaginate($paginate);
        return Post::where($search, $value);
    }

    public function simplePaginate($count)
    {
        return Post::simplePaginate($count);
    }

}