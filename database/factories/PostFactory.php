<?php

/*
|--------------------------------------------------------------------------
| Video Factory
|--------------------------------------------------------------------------
|
| This creates  a Video factory for seeding the database.
|
*/

$factory->define(Klipboard\Post::class, function (Faker\Generator $faker) {
    return [
        'description' => $faker->realText(),
        'content' => $faker->text(),
        'image_url' => $faker->url,
        'user_id' => function () {
            return factory(Klipboard\User::class)->create()->id;
        }
    ];
});