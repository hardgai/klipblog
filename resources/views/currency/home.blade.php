@extends('layouts.app')

@section('content')

    <style>

        .ui-autocomplete.ui-front.ui-menu li {
            list-style: none;
            font-weight: bold;
        }

        .ui-autocomplete.ui-front.ui-menu li:hover {
            background-color: #F5F5F5;
        }

        .title-box {
            font-size: larger;
            padding: 2em;
            background-color: #000;
            color: white;
            text-transform: capitalize;
            text-align: center;
            height: 150px;
        }

        .title-box:hover {
            cursor: pointer;
            background-color: white;
            border: 2px solid rgb(0, 100, 100);
            color: black;
        }

        .detail-box {
            padding: 0.5em;
            background-color: rgb(0, 100, 100);
            color: #fff;
            font-size: small;
        }

        .col-md-3.col-sm-3.col-xs-3 {
            margin-bottom: 1em;
        }

        #posts {
            margin-top: 2em;
        }

        @media (max-width: 640px) {
             .col-md-3.col-sm-3.col-xs-3 {
                width: 100%;
            }
        }
    </style>

    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-xs-12 col-md-8">
                <form action="{{ url('/currency/convert') }}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label>
                            From:
                        </label>
                        <select name="cur_1" class="form-control">
                            @foreach ( $currency as $key => $value )
                                <option value="{{$key}}">{{ $value['currencyName'] }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>
                            To:
                        </label>
                        <select name="cur_2" class="form-control">
                            @foreach ( $currency as $key => $value )
                                <option value="{{$key}}">{{ $value['currencyName'] }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>
                            Amount:
                        </label>
                        <input name="amount" placeholder="Amount" class="form-control">
                    </div>
                    <div class="">
                        <button type="submit" class="btn btn-primary">Convert</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
